﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatJPO_Serveur
{
    class Serveur
    {
        public const int PORT = 32320;

        private TcpListener serverListener;
        private Dictionary<string, TcpClient> clients;


        public Serveur()
        {

            clients = new Dictionary<string, TcpClient>();

            serverListener = new TcpListener(32321);
            serverListener.Start();


            while (true)
            {
                TcpClient client = serverListener.AcceptTcpClient();

                string pseudo = RécupérationPseudo(client);

                if (PseudoLibre(pseudo))
                {
                    EnvoieConnexionRéussie(client, pseudo);
                }
                else
                {
                    EnvoiePseudoDéjàUtilisé(client);
                }
            }
        }

        private string RécupérationPseudo(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            byte[] data = new byte[client.ReceiveBufferSize];
            stream.Read(data, 0, data.Length);

            string pseudo = System.Text.Encoding.ASCII.GetString(data);
            pseudo = pseudo.Substring(0, pseudo.IndexOf("/_FIN_"));

            return pseudo;
        }

        private bool PseudoLibre(string pseudo)
        {
            return !clients.Keys.Contains(pseudo);
        }

        private void EnvoieConnexionRéussie(TcpClient client, string pseudo)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("/_PSEUDO_OK_/_FIN_");
            clients.Add(pseudo, client);
            client.GetStream().Write(data, 0, data.Length);
            client.GetStream().Flush();
            new GestionClient().Start(client, pseudo, this);
        }

        private void EnvoiePseudoDéjàUtilisé(TcpClient client)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("/_PSEUDO_NON_OK_/_FIN_");
            client.GetStream().Write(data, 0, data.Length);
            client.GetStream().Flush();
        }

        public void EnvoieListeUtilisateursConnectés(TcpClient client)
        {
            string message = "/_UTILISATEUR_";
            foreach (KeyValuePair<string, TcpClient> cli in clients)
            {
                if (client != cli.Value)
                    message += cli.Key + ";";
            }
            message += "/_FIN_";


            byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            client.GetStream().Write(data, 0, data.Length);
            client.GetStream().Flush();
        }

        public void EnvoyerATous(string msg)
        {

            foreach (KeyValuePair<string, TcpClient> client in clients)
            {
                byte[] data = new byte[client.Value.SendBufferSize];
                data = System.Text.Encoding.ASCII.GetBytes(msg);
                client.Value.GetStream().Write(data, 0, data.Length);
                client.Value.GetStream().Flush();
            }
        }

        public void SupprimerClient(string pseudo)
        {
            clients.Remove(pseudo);
        }

        public Dictionary<string, TcpClient> Clients
        {
            get
            {
                return clients;
            }
        }
    }

    class GestionClient
    {
        private const int NB_MESSAGES_MAX = 5;

        private Serveur serveur;
        private TcpClient client;
        private string pseudo;
        private bool continuer;

        public void Start(TcpClient c, string pseudo, Serveur serveur)
        {
            continuer = true;
            client = c;
            this.pseudo = pseudo;
            this.serveur = serveur;
            Thread t = new Thread(Gestion);
            t.Start();
        }

        public void Gestion()
        {
            DateTime temps = DateTime.Now;
            int nbMessages = 0;

            while (continuer)
            {

                string message = AttendreMessageClient(client);

                if (tempsDAttenteDépassé(temps))
                {
                    temps = DateTime.Now;
                    nbMessages = 0;
                }

                if (message.Contains("/_FIN_"))
                {
                    string contenuMessage = message.Substring(0, message.IndexOf("/_FIN_"));

                    if (message.StartsWith("/_DECONNEXION_"))
                    {
                        continuer = false;
                        GérerDéconnexion(message);

                        Console.WriteLine(pseudo + " s'est déconnecté");
                    }
                    else if (message.StartsWith("/_CONNEXION_"))
                    {
                        serveur.EnvoyerATous(message);

                        contenuMessage = contenuMessage.Substring("/_CONNEXION_".Length, contenuMessage.Length - "/_CONNEXION_".Length);
                        Console.WriteLine(contenuMessage + " s'est connecté");
                    }
                    else if (message.StartsWith("/_UTILISATEURS_"))
                    {
                        serveur.EnvoieListeUtilisateursConnectés(client);

                        Console.WriteLine(pseudo + " récupère les utilisateurs");
                    }
                    else if (message.StartsWith("/_MESSAGE_"))
                    {
                        serveur.EnvoyerATous(message);

                        contenuMessage = contenuMessage.Substring("/_MESSAGE_".Length, contenuMessage.Length - "/_MESSAGE_".Length);
                        Console.WriteLine(contenuMessage);

                        nbMessages++;
                        if (nbMessages >= NB_MESSAGES_MAX)
                        {
                            continuer = false;
                            GérerKick("/_KICK_" + pseudo + "/_FIN_");

                            Console.WriteLine(pseudo + " a été éjecté");
                        }
                    }
                }
            }
        }

        private bool tempsDAttenteDépassé(DateTime temps)
        {
            bool tempsDépassé = false;
            if (DateTime.Now.Year - temps.Year > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Month - temps.Month > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Day - temps.Day > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Hour - temps.Hour > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Minute - temps.Minute > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Second - temps.Second > 0)
            {
                tempsDépassé = true;
            }
            else if (DateTime.Now.Millisecond - temps.Millisecond > 500)
            {
                tempsDépassé = true;
            }
            return tempsDépassé;
        }

        private string AttendreMessageClient(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            byte[] data = new byte[client.SendBufferSize];
            try
            {
                stream.Read(data, 0, data.Length);
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
                continuer = false;
                GérerDéconnexion("/_DECONNEXION_" + pseudo + "/_FIN_");
            }


            return System.Text.Encoding.ASCII.GetString(data);
        }

        private void GérerDéconnexion(string donnée)
        {
            serveur.SupprimerClient(pseudo);
            serveur.EnvoyerATous(donnée);
        }

        private void GérerKick(string donnée)
        {
            serveur.EnvoyerATous(donnée);
            serveur.SupprimerClient(pseudo);
        }
    }
}
