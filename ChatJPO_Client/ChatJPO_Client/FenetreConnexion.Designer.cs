﻿namespace ChatJPO_Client
{
    partial class FenetreConnexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.texteConnexion = new System.Windows.Forms.Label();
            this.texteNom = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.texteIP = new System.Windows.Forms.Label();
            this.boutonConnecter = new System.Windows.Forms.Button();
            this.texteEtat = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // texteConnexion
            // 
            this.texteConnexion.AutoSize = true;
            this.texteConnexion.Location = new System.Drawing.Point(13, 13);
            this.texteConnexion.Name = "texteConnexion";
            this.texteConnexion.Size = new System.Drawing.Size(63, 13);
            this.texteConnexion.TabIndex = 0;
            this.texteConnexion.Text = "Connexion :";
            // 
            // texteNom
            // 
            this.texteNom.AutoSize = true;
            this.texteNom.Location = new System.Drawing.Point(13, 65);
            this.texteNom.Name = "texteNom";
            this.texteNom.Size = new System.Drawing.Size(35, 13);
            this.texteNom.TabIndex = 1;
            this.texteNom.Text = "Nom :";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(54, 62);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(257, 20);
            this.textBoxNom.TabIndex = 2;
            this.textBoxNom.Text = "Entrez le pseudonyme";
            this.textBoxNom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AppuiSurEntrée);
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(54, 102);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(257, 20);
            this.textBoxIP.TabIndex = 3;
            this.textBoxIP.Text = "Entrez l\'adresse IP";
            this.textBoxIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AppuiSurEntrée);
            // 
            // texteIP
            // 
            this.texteIP.AutoSize = true;
            this.texteIP.Location = new System.Drawing.Point(13, 105);
            this.texteIP.Name = "texteIP";
            this.texteIP.Size = new System.Drawing.Size(23, 13);
            this.texteIP.TabIndex = 4;
            this.texteIP.Text = "IP :";
            // 
            // boutonConnecter
            // 
            this.boutonConnecter.Enabled = false;
            this.boutonConnecter.Location = new System.Drawing.Point(236, 139);
            this.boutonConnecter.Name = "boutonConnecter";
            this.boutonConnecter.Size = new System.Drawing.Size(75, 23);
            this.boutonConnecter.TabIndex = 5;
            this.boutonConnecter.Text = "OK";
            this.boutonConnecter.UseVisualStyleBackColor = true;
            this.boutonConnecter.Click += new System.EventHandler(this.boutonConnecter_Click);
            this.boutonConnecter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AppuiSurEntrée);
            // 
            // texteEtat
            // 
            this.texteEtat.AutoSize = true;
            this.texteEtat.ForeColor = System.Drawing.Color.Red;
            this.texteEtat.Location = new System.Drawing.Point(13, 139);
            this.texteEtat.Name = "texteEtat";
            this.texteEtat.Size = new System.Drawing.Size(0, 13);
            this.texteEtat.TabIndex = 6;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // FenetreConnexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 174);
            this.Controls.Add(this.texteEtat);
            this.Controls.Add(this.boutonConnecter);
            this.Controls.Add(this.texteIP);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.texteNom);
            this.Controls.Add(this.texteConnexion);
            this.Name = "FenetreConnexion";
            this.Text = "Connexion au chat";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AppuiSurEntrée);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label texteConnexion;
        private System.Windows.Forms.Label texteNom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label texteIP;
        private System.Windows.Forms.Button boutonConnecter;
        private System.Windows.Forms.Label texteEtat;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}