﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatJPO_Client
{
    public partial class FenetreConnexion : Form
    {
        public FenetreConnexion()
        {
            InitializeComponent();
        }

        private void boutonConnecter_Click(object sender, EventArgs e)
        {
            
        }

        private void DémarrerConnexion()
        {
            Client client = new Client();
            int erreur = client.Connexion(textBoxNom.Text, textBoxIP.Text);
            if (erreur == 0)
            {
                this.Hide();
                FenetreChat fen = new FenetreChat(client);
                fen.Show();
                fen.InitialiserConnexion();
            }
            else
            {
                texteEtat.Text = "Erreur inconnue !";
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void AppuiSurEntrée(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DémarrerConnexion();
            }
        }
    }
}
