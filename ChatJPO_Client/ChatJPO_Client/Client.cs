﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatJPO_Client
{
    public class Client
    {
        private const int CONNEXION_REUSSIE = 0;
        private const int CONNEXION_ECHOUEE = 1;
        private const int PSEUDO_INDISPONNBILE = 2;

        private TcpClient socketClient;

        private NetworkStream stream;
        String pseudo;
        FenetreChat fenetreChat;
        Thread reception;
        bool receptionPrete = false;


        public Client() { }

        public int Connexion(String pseudo, String ip)
        {
            this.pseudo = pseudo;
            this.socketClient = new TcpClient();
            int retour = CONNEXION_ECHOUEE;
            string msg = "";
            try
            {
                if (socketClient.ConnectAsync(ip, 32321).Wait(2000))
                {
                    stream = socketClient.GetStream();
                    DemanderPseudo(pseudo);
                    msg = RécupérerDisponibilitéPseudo();

                    if (PseudoDisponnible(msg))
                    {
                        EnvoyerConnexion(pseudo);
                        retour = CONNEXION_REUSSIE;
                    }
                    else
                    {
                        retour = PSEUDO_INDISPONNBILE;

                    }

                }
            }
            catch (System.AggregateException e)
            {
                Console.WriteLine(e.Message);
            }

            return retour;

        }

        private void EnvoyerConnexion(string pseudo)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("/_CONNEXION_" + pseudo + "/_FIN_");

            stream.Write(data, 0, data.Length);
            stream.Flush();
        }

        private bool PseudoDisponnible(string msg)
        {
            return msg.StartsWith("/_PSEUDO_OK_");
        }

        private string RécupérerDisponibilitéPseudo()
        {
            byte[] data = new byte[socketClient.ReceiveBufferSize];
            stream.Read(data, 0, data.Length);
            string msg = Encoding.ASCII.GetString(data);

            return msg.Substring(0, msg.IndexOf("/_FIN_"));
        }

        public void DemanderPseudo(string pseudo)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(pseudo + "/_FIN_");
            stream.Write(data, 0, data.Length);
            stream.Flush();
        }

        public void DemanderUtilisateurs()
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("/_UTILISATEURS_/_FIN_");
            stream.Write(data, 0, data.Length);
            stream.Flush();
        }

        public void InitialiserConnexion(FenetreChat fenetreChat)
        {
            this.fenetreChat = fenetreChat;

            reception = new Thread(Reception);
            reception.Start();
            while (!receptionPrete) ;
            fenetreChat.RecupérerUtilisateurs();

        }

        public void FermerConnexion()
        {
            EnvoyerDéconnexion();
            socketClient.Close();
            Environment.Exit(0);
        }

        public void EnvoyerMessage(String message)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("/_MESSAGE_" + pseudo + ": " + "/_FIN_");

            stream.Write(data, 0, data.Length);
            stream.Flush();
        }

        public void EnvoyerDéconnexion()
        {
            try
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes("/_DECONNEXION_" + pseudo + "/_FIN_");

                stream.Write(data, 0, data.Length);
                stream.Flush();
            }
            catch (System.IO.IOException e)
            {

            }

        }

        private void Reception()
        {
            bool pasDErreur = true;
            while (pasDErreur)
            {
                try
                {
                    receptionPrete = true;
                    string donnée = AttendreMessageServeur();
                    string contenuDonnée;

                    if (donnée.StartsWith("/_UTI$ùATEUR_"))
                    {
                        contenuDonnée = RécupérerDonnéeSansEnTête(donnée, "/_UTILISATEUR_");

                        string[] noms = contenuDonnée.Split(';');
                        foreach (String nom in noms)
                        {
                            if (nom.Length > 0)
                                AjouterUtilisateur(nom);
                        }
                    }
                    else if (donnée.StartsWith("/_CONNEXION_"))
                    {
                        contenuDonnée = RécupérerDonnéeSansEnTête(donnée, "/_CONNEXION_");

                        AjouterMessage(contenuDonnée + " s'est connecté");
                        AjouterUtilisateur(contenuDonnée);
                    }
                    else if (donnée.StartsWith("/_DECONNEXION_"))
                    {
                        contenuDonnée = RécupérerDonnéeSansEnTête(donnée, "/_DECONNEXION_");

                        SupprimerUtilisateur(contenuDonnée);
                        AjouterMessage(contenuDonnée + " s'est déconnecté");
                    }
                    else if (donnée.StartsWith("/_KICK_"))
                    {
                        contenuDonnée = RécupérerDonnéeSansEnTête(donnée, "/_KICK_");

                        if (contenuDonnée == pseudo)
                        {
                            MessageBox.Show("Vous avez été éjecté pour spam", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            socketClient.Close();
                            Environment.Exit(0);
                        }

                        SupprimerUtilisateur(contenuDonnée);
                        AjouterMessage(contenuDonnée + " a été éjecté");
                    }
                    else if (donnée.StartsWith("/_MESSAGE_"))
                    {
                        if (!fenetreChat.Spammage)
                        {
                            contenuDonnée = RécupérerDonnéeSansEnTête(donnée, "/_MESSAGE_");

                            AjouterMessage(contenuDonnée);
                        }
                    }

                }
                catch (System.IO.IOException e)
                {
                    pasDErreur = false;
                }
            }
        }

        private string AttendreMessageServeur()
        {
            byte[] data = new byte[socketClient.ReceiveBufferSize];
            stream.Read(data, 0, data.Length);

            string donnée = System.Text.Encoding.ASCII.GetString(data);
            if (donnée.IndexOf("/_FIN_") != -1)
                donnée = donnée.Substring(0, donnée.IndexOf("/_FIN_"));
            return donnée;
        }

        private static string RécupérerDonnéeSansEnTête(string msg, string enTête)
        {
            return msg.Substring(enTête.Length, msg.Length - enTête.Length);
        }

        private void SupprimerUtilisateur(string msg)
        {
            fenetreChat.BeginInvoke(
                                    (Action)(() =>
                                    {
                                        fenetreChat.supprimerUtilisateur(msg);
                                    }));
        }

        private void AjouterUtilisateur(string msg)
        {
            fenetreChat.BeginInvoke(
                                    (Action)(() =>
                                    {
                                        fenetreChat.ajouterUtilisateur(msg);
                                    }));
        }

        private void AjouterMessage(string msg)
        {
            fenetreChat.BeginInvoke(
                                    (Action)(() =>
                                    {
                                        fenetreChat.ajouterMessage(msg);
                                    }));
        }
    }
}
