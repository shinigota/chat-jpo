﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatJPO_Client
{
    public partial class FenetreChat : Form
    {
        private Client client;
        List<string> utilisateurs;
        Thread spamThread;
        string messageSpam;

        public FenetreChat(Client client)
        {
            InitializeComponent();
            utilisateurs = new List<string>();
            this.client = client;
            messageSpam = "aha";
        }

        public void InitialiserConnexion()
        {
            this.client.InitialiserConnexion(this);
            boutonEnvoyer.Enabled = false;
        }

        private void boutonEnvoyer_Click(object sender, EventArgs e)
        {
            client.EnvoyerMessage(textBoxMessage.Text);
            textBoxMessage.Clear();
            boutonEnvoyer.Enabled = false;
        }

        public void ajouterMessage(string message)
        {
            string pseudo = message.Split(' ')[0];
            string contenuMessage = message.Substring(pseudo.Length, message.Length - pseudo.Length);
            textBoxChat.SelectionFont = new Font(textBoxChat.Font, FontStyle.Bold | FontStyle.Underline);
            textBoxChat.AppendText(pseudo);
            textBoxChat.SelectionFont = new Font(textBoxChat.Font, FontStyle.Regular);
            textBoxChat.AppendText(contenuMessage + "\r\n");
        }

        public void supprimerUtilisateur(string msg)
        {
            if (utilisateurs.Contains(msg))
            {
                utilisateurs.Remove(msg);

                textBoxUtilisateurs.Clear();

                foreach (string pseudo in utilisateurs)
                    textBoxUtilisateurs.AppendText(pseudo + "\r\n");
            }
        }

        public void ajouterUtilisateur(string msg)
        {
            if (!utilisateurs.Contains(msg))
            {
                utilisateurs.Add(msg);
                textBoxUtilisateurs.AppendText(msg + "\r\n");
            }

        }

        private void FenetreChat_FormClosing(object sender, FormClosingEventArgs e)
        {
            client.FermerConnexion();
        }

        public void RecupérerUtilisateurs()
        {
            client.DemanderUtilisateurs();

        }

        private void textBoxMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBoxMessage.Text.Length <= 1 && e.KeyChar == (char)Keys.Back)
                boutonEnvoyer.Enabled = false;
            else if (e.KeyChar != (char)Keys.Enter)
                boutonEnvoyer.Enabled = true;
        }

        private void textBoxMessage_TextChanged(object sender, EventArgs e)
        {
            string texte = "";

            foreach (string ligne in textBoxMessage.Lines)
                if (ligne.Length > 0)
                    texte += ligne;

            textBoxMessage.Text = texte;
        }

        private void boutonQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Spam_Click(object sender, EventArgs e)
        {
            if (Spam.Checked)
            {
                //Code pour activer le spam.
            }
            else
            {
                //Code pour désactiver le spam.
            }
        }

        private void Spammeur()
        {
            while (true)
            {
                
            }
        }

        public bool Spammage
        {
            get
            {
                return Spam.Checked;
            }
        }
    }

}
